const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/YouTube');

const videoSchema = new mongoose.Schema({
    title: String,
    description: String,
    publish_datetime: Date,
    thumbnail: String
});

const Video = mongoose.model('videos', videoSchema);

module.exports = {
    Video
};