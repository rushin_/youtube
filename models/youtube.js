const axios = require('axios');
const { Video } = require('./mongo');

// API key of YouTube data V3 API.
// Document to generate API key https://developers.google.com/youtube/v3/getting-started
// Backup key: AIzaSyDh-6Lxrb5G7orQ-P71rYkkWz5QprOykk0
const key = 'AIzaSyBcaloOCLtny9oLoktBO2WNVXcm0IdsjSg';

// This function gets the list of videos from YouTube data v3 API on the 
// basis of query, date and number of pages and populate it in the DB.
const populateVideosInDB = async (query, date, pages) => {
    let listOfVideos, pageToken = null;
    for (let i = 0; i < pages; i++) {
        ({listOfVideos, pageToken} = await getVideosFromYouTube(query, date, pageToken));
        insertVideosInDB(listOfVideos);
    }
}

// This function takes in date, pageToken and query and on the basis of it
// returns (listOfVideos, nextPageToken) the list of videos and token of next page 
// using YouTube data v3 APi.
const getVideosFromYouTube = async (query, date, pageToken = null) => {
    const config = {
        method: 'get',
        url: `https://youtube.googleapis.com/youtube/v3/search?${pageToken ? `&pageToken=${pageToken}&` : ``}part=snippet&maxResults=25&publishedAfter=${date}&q=${query}&key=${key}`,
        headers: {
            'Accept': 'application/json'
        }
    }
    const response = await axios(config);
    return response.status = '200'
        ? {listOfVideos: response.data.items, pageToken: response.data.nextPageToken}
        : null;
}

// This function takes in array of videos and insert it to the videos collection in DB.
const insertVideosInDB = (videos) => {
    videos.forEach(element => {
        new Video({
            title: element.snippet.title,
            description: element.snippet.description,
            publish_datetime: element.snippet.publishTime,
            thumbnail: element.snippet.thumbnails.high.url
        }).save();
    });
}

// Returns videos on the basis of the query, filter, start, numberOfVideos and sortAscending.
const getVideos = async (query, filter, start = 1, offset = 10, sortAscending = false) => {
    return await Video.find(filter == 'title' ? { title: new RegExp(query, 'i') }
                                              : { description: new RegExp(query, 'i') })
        .select({ "_id": 0, "title": 1, "description": 1, "publish_datetime": 1, "thumbnail": 1 })
        .sort({ publish_datetime: sortAscending ? -1 : 1})
        .skip(start)
        .limit(offset);
}

module.exports = {
    getVideos,
    populateVideosInDB,
}


// Unused code please ignore.

// // This function has 1 parameter 'query' and on the basis of that parameter 
// // it will do a partial match title on description and return the array of videos.
// const getVideosByTitle = async (query, start, numberOfVideos, sortAscending) => {
//     return await Video.find({ title: new RegExp(query, 'i') })
//         .select({ "_id": 0, "title": 1, "description": 1, "publish_datetime": 1, "thumbnail": 1 })
//         .sort({ publish_datetime: sortAscending })
//         .skip(start)
//         .limit(numberOfVideos);
// }

// // This function has 1 parameter 'query' and on the basis of that parameter 
// // it will do a partial match on description and return the array of videos.
// const getVideosByDescription = async (query, start, numberOfVideos, sortAscending) => {
//     return await Video.find({ description: new RegExp(query, 'i') })
//         .select({ "_id": 0, "title": 1, "description": 1, "publish_datetime": 1, "thumbnail": 1 })
//         .sort({ publish_datetime: sortAscending })
//         .skip(start)
//         .limit(numberOfVideos);
// }

// // This function has 2 parameters start and numberOfVideos and 
// // return a list of videos starting from the index 'start' 
// // containing the number of videos mentioned in 'numberOfVideos'.
// const getPaginatedVideos = async (start, numberOfVideos, sortAscending) => {
//     return await Video.find({})
//         .select({"_id": 0, "title": 1, "description": 1, "publish_datetime": 1, "thumbnail": 1})
//         .sort({ publish_datetime: sortAscending })
//         .skip(start)
//         .limit(numberOfVideos)
// }

// getVideosByTitle,
    // getVideosByDescription,
    // getPaginatedVideos