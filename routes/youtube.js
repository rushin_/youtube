const express = require('express');
const router = express.Router();
const { getVideos } = require('../models/youtube');
const Joi = require('joi');
const _ = require('lodash');

router.get('/paginated-videos', async (req, res) => {
    const schema = Joi.object({
        query: Joi.string().required(),
        filter: Joi.string().required().valid('title', 'description'),
        start: Joi.number(),
        offset: Joi.number(),
        sort_ascending: Joi.bool()
    });

    const { error } = schema.validate(req.body);

    if (error) {
        return res.publish(false, _.get(error, 'details[0].message')
            + ' | Path: ' + _.get(error, 'details[0].path'));
    }

    try {
        videos = await getVideos(req.body.query, req.body.filter, req.body.start,
                                 req.body.offset, req.body.sort_ascending);
        return res.publish(true, 'Success', videos);
    } catch (e) {
        console.error('Error in getting paginated videos: ', e);
        return res.publish(false, 'Error in getting videos');
    }
})

module.exports = router;


// Umused code please ignore.

// videos = await getVideos(!req.body.query ? null : req.body.query,
//     !req.body.filter ? null : req.body.filter,
//     !req.body.start ? 1 : req.body.start,
//     !req.body.offset ? 10 : req.body.offset,
//     !req.body.sort_ascending ? -1 : req.body.sort_ascending);

/* getVideosByTitle, getVideosByDescription, getPaginatedVideos, */


// const filter = {
//     "blank": {},
//     "title": { title: new RegExp(query, 'i') },
//     "description": { description: new RegExp(query, 'i') },
// };
// Object.freeze(Color);

// router.get('/video-by-title', async (req, res) => {
//     if (!req.body.query)
//         return res.publish(false, 'query is required.')
//     try {
//         videos = await getVideosByTitle(req.body.query);
//         return res.publish(true, 'Success', videos);
//     } catch (e) {
//         console.error('Error in getting videos by title: ', e);
//         return res.publish(false, 'Error in getting videos by title.');
//     }
// })

// router.get('/video-by-description', async (req, res) => {
//     if (!req.body.query)
//         return res.publish(false, 'query is required.')
//     try {
//         videos = await getVideosByDescription(req.body.query)
//         return res.publish(true, 'Success', videos);
//     } catch (e) {
//         console.error('Error in getting videos by description: ', e);
//         return res.publish(false, 'Error in getting videos');
//     }
// })
