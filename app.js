const express = require('express'), youtube = require('./routes/youtube');
const cron = require('node-cron')
const { populateVideosInDB } = require('./models/youtube')
const app = express()
const port = 7777

app.use(express.json())

app.use(function (req, res, next) {
    res.publish = function (status, messsage, data = null) {
        return res.json({
            status: status,
            messsage: messsage,
            data: data
        })
    };
    next();
});

app.use('/youtube', youtube);

// cron.schedule('*/30 * * * * *', async () => {
//     try {
//         const lastYearDate = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
//         const search = 'mongodb';
//         populateVideosInDB(search, lastYearDate.toISOString(), 10);
//     } catch (e) {
//         console.log(e);
//     }
// })

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
})